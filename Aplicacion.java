package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Baldemar on 15/07/2017.
 */
public class Aplicacion {
    private List<Producto> listaPrdoucto;
    private List<Cliente> listaCliente;

    public Aplicacion() {
        listaPrdoucto = new ArrayList<>();
        listaCliente = new ArrayList<>();
    }

    public List<Producto> getListaPrdoucto() {
        return listaPrdoucto;
    }

    public void setListaPrdoucto(List<Producto> listaPrdoucto) {
        this.listaPrdoucto = listaPrdoucto;
    }

    public List<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    public void addProducto(Producto p){
        listaPrdoucto.add(p);
    }
    public void addCliente(Cliente c){
        listaCliente.add(c);
    }
}
