package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import clases.Aplicacion;
import clases.Cliente;
import clases.Producto;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {
    Aplicacion app = new Aplicacion();
    private Grid<Producto> gridP = new Grid<>("Productos");
    private Grid<Cliente> gridC = new Grid<>("Clientes");


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        final TextField nombreProducto = new TextField("Nombre Producto");
        final TextField precioP = new TextField("Precio Producto");

        final TextField nombreCliente = new TextField("Nombre Cliente");
        final TextField direccionCliente = new TextField("Direccion Cliente");
        final TextField telefonoCliente = new TextField("Telefono Cliente");

        Button button3 = new Button("Registrar Producto");
        button3.addClickListener( e -> {
            Producto pro = new Producto();
            pro.setNombre(nombreProducto.getValue());
            pro.setPrecio(Double.parseDouble(precioP.getValue()));
            app.addProducto(pro);
            Notification.show("Agregado");
            gridP.setItems(app.getListaPrdoucto());
            nombreProducto.clear();
            precioP.clear();
            gridP.setVisible(false);
        });

        Button button5 = new Button("Registrar Cliente");
        button5.addClickListener( e -> {
            Cliente cli = new Cliente();
            cli.setNombre(nombreCliente.getValue());
            cli.setDireccion(direccionCliente.getValue());
            cli.setTelefono(Integer.parseInt(telefonoCliente.getValue()));
            app.addCliente(cli);
            Notification.show("Agregado");
            gridC.setItems(app.getListaCliente());
            nombreCliente.clear();
            direccionCliente.clear();
            telefonoCliente.clear();
            gridC.setVisible(false);
        });

        Button button1 = new Button("Ingresar Productos");
        button1.addClickListener( e -> {
            layout.addComponents(nombreProducto,precioP,button3);

        });
        Button button2 = new Button("Ingresar Cliente");
        button2.addClickListener( e -> {
            layout.addComponents(nombreCliente,direccionCliente,telefonoCliente,button5);
            gridP.setVisible(false);

                });

        Button button4 = new Button("Mosrar Clientes");
        button4.addClickListener( e -> {

            gridC.setVisible(true);
            nombreCliente.setVisible(false);
            direccionCliente.setVisible(false);
            telefonoCliente.setVisible(false);
            button5.setVisible(false);

        });

        Button button6 = new Button("Mosrar Producto");
        button6.addClickListener( e -> {

            gridP.setVisible(true);
            nombreProducto.setVisible(false);
            precioP.setVisible(false);
            button3.setVisible(false);

        });
        gridP.addColumn(Producto::getNombre).setCaption("Nombre");
        gridP.addColumn(Producto::getPrecio).setCaption("Precio");
        layout.addComponents(button1,button6,button2,button4);
        layout.addComponent(gridP);
        gridP.setVisible(false);
        setContent(layout);

        gridC.addColumn(Cliente::getNombre).setCaption("Nombre");
        gridC.addColumn(Cliente::getDireccion).setCaption("Direccion");
        gridC.addColumn(Cliente::getTelefono).setCaption("Telefono");
        layout.addComponent(gridC);
        gridC.setVisible(false);
        setContent(layout);

    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
